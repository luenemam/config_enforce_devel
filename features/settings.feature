@settings @api
Feature: Settings.
  In order to enforce configs
  As an Administrator
  I need to be able to view, create, edit and save config enforcement settings.

  Background:
    Given I am logged in as a user with the "administer site configuration" permission

  Scenario: Navigate to Devel Settings
     When I go to "admin/config/development/config_enforce/enforced_configs"
      And I click "Settings"
     Then I should see "Config form:"
      And I should see "Enable enforcement of the config from this form."

  @wip
  Scenario: Select Automated Cron module for config output
     When I go to "admin/config/development/config_enforce/enforced_configs"
      And I click "Settings"
      And I select "Automated Cron" from "Available Options"
      And I press "Save configuration"
     Then I should see "The configuration options have been saved."

  Scenario: Unhide module config options
     When I go to "admin/config/system/site-information"
      And I check the box "Enforce config"
     Then I should see " The module to which config should be written."
