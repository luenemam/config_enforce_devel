@delete @api
Feature: Delete enforced configs.
  In order to delete config entities that are being enforced
  As a developer
  I need enforced config settings to be deleted along with the config objects.

  Background:
    Given I am logged in as a user with the "administer site configuration, administer node fields" permissions

  @javascript
  Scenario:
    Given I go to "/admin/structure/types/manage/article/fields/add-field"
      And I select "Boolean" from "Add a new field"
      And I enter "TEST_FIELD" for "Label"
      And I wait for AJAX to finish
     When I press the "Save and continue" button
     Then I should be on "/admin/structure/types/manage/article/fields/node.article.field_test_field/storage"
      And I should see "These settings apply to the TEST_FIELD field everywhere it is used."
     When I press "Save field settings"
     Then I should see "Updated field TEST_FIELD field settings."
     When I check the box "Enforce config"
      And I press "Save configuration"
      And I go to "admin/config/development/config_enforce/enforced_configs"
     Then I should see the link "field.field.node.article.field_test_field"
      And I should see "field.storage.node.field_test_field"
     When I go to "admin/structure/types/manage/article/fields/node.article.field_test_field/delete"
      And I press "Delete"
     Then I should see "The field TEST_FIELD has been deleted from the Article content type."
     When I go to "admin/config/development/config_enforce/enforced_configs"
     Then I should not see the link "field.field.node.article.field_test_field"
      And I should not see "field.storage.node.field_test_field"

