---
title: Basic Usage
weight: 30

---

You now have the basic setup of Config Enforce Devel, with one or more Target Modules configured, and some default enforcement settings in place. From here, you can proceed to go about your typical site building tasks, enforcing things as you go. As a simple example, to put the `system.site` config object under enforcement:

1. Go to *Admin >> Config >> System >> Basic site settings* (*admin/config/system/site-information*), and ensure the *Site Name*, *Email*, etc. are configured.
    1. You will see the Config Enforce "floaty box" widget appear in the top-right corner.
2. Check the Enforce config checkbox, and validate the *Target module*, *Directory*, and *Enforcement level* are correct (usually the defaults are fine, but you may want to move certain configs into different target modules).
3. Click Save configuration, and observe the form highlights the config object with a colour to indicate enforement level (green is read-only).
    1. If you look at the filesystem for your project, you will see a newly-written `system.site.yml` in the Target Module you selected, as well as an update to the `config_enforce.registry.*.yml` reflecting the newly-enforced config.
    2. Note that no `drush cex` or other manual intervention was required to export the config object- any subsequent changes to this config form will immediately be reflected on the filesystem.
4. `git add` and `git commit` both the `config_enforce.registry` as well as the `system.site` YAML files to capture them in your project repository.
5. Go back to the *Basic site settings* form and make a change, then use `git status` to observe the corresponding change immediately appear on the filesystem.
    1. Note that both the config object YAML file as well as the registry are updated with each change, since the content hash for the config object is updated to reflect the current state.

From here you can carry on with site-building tasks, checking "Enforce config" each time you get to a config form that hasn't yet been enforced, and periodically committing the various config objects and registries to reflect those changes.


