---
title: "Docs"

---

Config Enforce Documentation
============================

Config Enforce
--------------

Config Enforce is intended to be used on production sites to ensure that configuration can't be altered directly. It does this by blocking changes to the active config and reloading configuration from the filesystem, if it has drifted.


Config Enforce Devel
--------------------

Config Enforce Devel is intended to be used on development sites to manage what configuration should be enforced by [Config Enforce](https://drupal.org/project/config_enforce).

[Config Enforce Devel](https://drupal.org/project/config_enforce_devel) is the companion module which you install only in your development environment, and allows you to manage what config will be enforced. It leverages [Config Devel](https://drupal.org/project/config_devel) to automatically read and write configuration to the filesystem as soon as they've changed.

*N.B.* Config Enforce Devel should *only* be installed in development environments. See [Security Model](reference/security-model) for details.

Documentation Index
-------------------

Use the menu on the left to navigate, or choose a page from the index below.

{{% children depth=2 %}}
