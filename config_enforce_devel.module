<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\config_enforce_devel\ToolbarHandler;
use Drupal\config_enforce_devel\ConfigDevelHelper;
use Drupal\config_enforce_devel\Form\EmbeddedEnforceForm;
use Drupal\config_enforce_devel\Form\ModalEnforceForm;
use Drupal\config_enforce_devel\FormHandler\DevelEnforceFormHandler;

/**
 * Implements hook_form_alter().
 */
function config_enforce_devel_form_alter(&$form, FormStateInterface &$form_state, $form_id) {
  (new DevelEnforceFormHandler($form, $form_state))->alter();
}

/**
 * Implements hook_config_devel_auto_import().
 */
function config_enforce_devel_config_devel_auto_import() {
  return ConfigDevelHelper::autoImport();
}

/**
 * Implements hook_config_devel_post_auto_import().
 */
function config_enforce_devel_config_devel_post_auto_import($auto_import) {
  ConfigDevelHelper::postAutoImport($auto_import);
}

/**
 * Implements hook_config_devel_auto_export().
 */
function config_enforce_devel_config_devel_auto_export() {
  return ConfigDevelHelper::autoExport();
}

/**
 * Implements hook_config_enforce_form_denylist().
 */
function config_enforce_devel_config_enforce_form_denylist() {
  return [
    EmbeddedEnforceForm::FORM_ID,
    ModalEnforceForm::FORM_ID,
  ];
}

/**
 * Implements hook_toolbar().
 */
function config_enforce_devel_toolbar() {
  return (new ToolbarHandler())->toolbar();
}
