<?php

namespace Drupal\config_enforce_devel\Form;

use Drupal\config_enforce\ConfigEnforcer;
use Drupal\config_enforce\EnforcedConfig;
use Drupal\config_enforce\Form\FormHelperTrait;
use Drupal\config_enforce_devel\EnforcedConfigCollection;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\Config\InstallStorage;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a settings form for the Config Enforce Devel module.
 */
trait DevelFormHelperTrait {

  use FormHelperTrait {
    setSharedFormProperties as defaultSetSharedFormProperties;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->setSharedFormProperties($form, $form_state);

    // Settings for registry configs are read-only.
    if ($this->isRegistryConfig()) return;

    $this->saveEnforcedConfigs();

    return parent::submitForm($this->form(), $this->formState());
  }

  /**
   * {@inheritdoc}
   */
  protected function setSharedFormProperties(array &$form, FormStateInterface &$form_state) {
    $this->defaultSetSharedFormProperties($form, $form_state);
    $this->enforcedConfigCollection = new EnforcedConfigCollection();
    return $this;
  }

  /**
   * Return an enforced config collection.
   */
  protected function getEnforcedConfigCollection() {
    return $this->enforcedConfigCollection;
  }

  /**
   * Add fields for a single config object.
   */
  protected function addEnforcedConfigItem($config) {
    parent::addEnforcedConfigItem($config);

    $this
      ->addEnforcedConfigEnabledField()
      ->overrideTargetModuleField()
      ->overrideConfigDirectoryField()
      ->overrideEnforcementLevelField()
      ->addEnforceDependenciesFields()
      ->setEnforcedConfigPathStates()
      ->makeRegistryConfigsReadOnly();
  }

  /**
   * Handle special case of enforcing enforced config registries.
   *
   * Since `config_enforce.enforced_configs.*` are a registry of settings for
   * enforced configs within a target module, they need to be hardcoded and
   * read-only, in order to avoid breakage.
   *
   * @see AddModuleForm::initializeTargetModuleEnforcedConfigs().
   * @see AddModuleForm::writeTargetModuleEnforcedConfigs().
   */
  protected function makeRegistryConfigsReadOnly() {
    if ($this->isRegistryConfig()) {
      $config = $this->getCurrentConfig();
      // Enforced configs settings must be read-only to avoid breakage.
      $this->form()['config_enforce'][$config]['config_enforce_enabled']['#attributes']['disabled'] = 'disabled';
      $this->form()['config_enforce'][$config]['target_module']['#attributes']['disabled'] = 'disabled';
      $this->form()['config_enforce'][$config]['config_directory']['#attributes']['disabled'] = 'disabled';
      $this->form()['config_enforce'][$config]['enforcement_level']['#attributes']['disabled'] = 'disabled';
    }
  }

  /**
   * Determine whether the current config object is an enforced config registry.
   */
  protected function isRegistryConfig() {
    $registry_names = $this
      ->getEnforcedConfigCollection()
      ->getTargetModuleCollection()
      ->getRegistryConfigNames();
    return in_array($this->getCurrentConfig(), $registry_names);
  }

  /**
   * Add a checkbox to control whether the current config object should be enforced.
   */
  protected function addEnforcedConfigEnabledField() {
    $this->form()['config_enforce'][$this->getCurrentConfig()]['config_enforce_enabled'] = [
      '#type' => 'checkbox',
      '#title' => 'Enforce config',
      '#description' => "Enable enforcement of the config from this form.",
      '#default_value' => $this->currentConfigIsEnforced(),
      '#attributes' => [
        'config-enforce-enabled' => $this->getEnabledAttribute(),
      ],
    ];
    return $this;
  }

  /**
   * Add a field to allow the target module to be selected.
   */
  protected function overrideTargetModuleField() {
    $config = $this->getCurrentConfig();
    $this->form()['config_enforce'][$config]['target_module'] = $this->getTargetModuleField();
    $this->form()['config_enforce'][$config]['target_module']['#states'] = $this->getEnabledVisibilityState();
    if (EnforcedConfig::isEnforced($config)) {
      $module = EnforcedConfig::getTargetModule($config);
      $this->form()['config_enforce'][$config]['target_module']['#title'] .= ' (<em>' . $module . '</em>)';
      $this->form()['config_enforce'][$config]['target_module']['#default_value'] = $module;
    }
    return $this;
  }

  /**
   * Add a field to allow the config directory to be selected.
   */
  protected function overrideConfigDirectoryField() {
    $config = $this->getCurrentConfig();
    $this->form()['config_enforce'][$config]['config_directory'] = $this->getConfigDirectoryField();
    $this->form()['config_enforce'][$config]['config_directory']['#states'] = $this->getEnabledVisibilityState();
    if (EnforcedConfig::isEnforced($config)) {
      $directory = EnforcedConfig::getConfigDirectory($config);
      $this->form()['config_enforce'][$config]['config_directory']['#title'] .= ' (<em>' . $directory . '</em>)';
      $this->form()['config_enforce'][$config]['config_directory']['#default_value'] = $directory;
    }
    return $this;
  }

  /**
   * Add a field to allow the enforcement level directory to be specified.
   */
  protected function overrideEnforcementLevelField() {
    $config = $this->getCurrentConfig();
    $this->form()['config_enforce'][$config]['enforcement_level'] = $this->getEnforcementLevelField();
    $this->form()['config_enforce'][$config]['enforcement_level']['#states'] = $this->getEnabledVisibilityState();
    if ($this->currentConfigIsEnforced()) {
      $this->form()['config_enforce'][$config]['enforcement_level']['#title'] .= ' (<em>' . EnforcedConfig::getEnforcementLevelLabel($config) . '</em>)';
      $this->form()['config_enforce'][$config]['enforcement_level']['#default_value'] = EnforcedConfig::getEnforcementLevel($config);
    }
    return $this;
  }

  /**
   * Add a checkbox to control whether to enforce dependent config objects.
   */
  protected function addEnforceDependenciesFields() {
    $dependencies = $this->getConfigDependencies();
    if (!count($dependencies) > 0) return $this;

    $this->form()['config_enforce'][$this->getCurrentConfig()]['enforce_dependencies'] = $this->getEnforceDependenciesField();
    $this->form()['config_enforce'][$this->getCurrentConfig()]['enforce_dependencies']['#attributes'] = [
      'config-enforce-dependencies' => $this->getEnforceDependenciesAttribute(),
    ];
    $this->form()['config_enforce'][$this->getCurrentConfig()]['enforce_dependencies']['#states'] = $this->getEnabledVisibilityState();

    $this->form()['config_enforce'][$this->getCurrentConfig()]['dependencies'] = [
      '#type' => 'container',
      '#states' => $this->getEnforceDependenciesVisibilityState(),
    ];
    $enforced_dependencies = 0;
    foreach ($dependencies as $dependency) {
      $enforced = EnforcedConfig::isEnforced($dependency);
      if ($enforced) $enforced_dependencies++;

      $this->form()['config_enforce'][$this->getCurrentConfig()]['dependencies'][$dependency] = [
        '#type' => 'checkbox',
        '#title' => $dependency . ($enforced ? $this->t(' (already enforced)') : ''),
        '#default_value' => FALSE,
        '#states' => [
          'checked' => [
            ':input[config-enforce-dependencies="' . $this->getEnforceDependenciesAttribute() . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }
    if ($enforced_dependencies > 0) {
      $this->disableEnforcedDependencies();
    }
    return $this;
  }

  /**
   * Disable checkboxes for dependencies that are already enforced, but allow for them to be overridden.
   */
  protected function disableEnforcedDependencies() {
    $this->form()['config_enforce'][$this->getCurrentConfig()]['override_existing_dependencies'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Override existing enforcement settings'),
      '#description' => $this->t('Allow existing enforcement settings for dependencies to be overridden by the settings from this form.'),
      '#default_value' => FALSE,
      '#attributes' => [
        'config-enforce-override-dependencies' => $this->getEnforceDependenciesAttribute(),
      ],
      '#states' => $this->getEnforceDependenciesVisibilityState(),
    ];

    foreach ($this->getConfigDependencies() as $dependency) {
      if (!EnforcedConfig::isEnforced($dependency)) continue;
      $this->form()['config_enforce'][$this->getCurrentConfig()]['dependencies'][$dependency]['#states'] = [
        'enabled' => [
          ':input[config-enforce-override-dependencies="' . $this->getEnforceDependenciesAttribute() . '"]' => ['checked' => TRUE],
        ],
      ];
    }
  }

  /**
   * Set the Form API '#states' setting for the config file path.
   */
  protected function setEnforcedConfigPathStates() {
    if ($this->currentConfigIsEnforced()) {
      $this->form()['config_enforce'][$this->getCurrentConfig()]['config_enforce_path']['#states'] = $this->getEnabledVisibilityState();
    }
    return $this;
  }

  /**
   * Determine whether the current config object is being enforced.
   */
  protected function currentConfigIsEnforced() {
    return EnforcedConfig::isEnforced($this->getCurrentConfig());
  }

  /**
   * Return the config-specific "enabled" attribute value.
   *
   * @see addEnforcedConfigEnabledField()
   * @see getEnabledVisibilityState()
   */
  protected function getEnabledAttribute() {
    return 'config_enforce_enabled-' . $this->getCurrentConfig();
  }

  /**
   * Return the Form API '#states' setting for config fields.
   *
   * This makes a given config's fields' visibility depend on the 'enabled' field for that config.
   */
  protected function getEnabledVisibilityState() {
    return [
      'invisible' => [
        ':input[config-enforce-enabled="' . $this->getEnabledAttribute() . '"]' => ['checked' => FALSE],
      ],
    ];
  }

  /**
   * Return the config-specific "enabled" attribute value.
   *
   * @see foo()
   * @see getEnforceDependenciesVisibilityState()
   */
  protected function getEnforceDependenciesAttribute() {
    return 'config_enforce_dependencies-' . $this->getCurrentConfig();
  }

  /**
   * Return the Form API '#states' setting for config fields.
   *
   * This makes a given config's fields' visibility depend on the 'enabled' field for that config.
   */
  protected function getEnforceDependenciesVisibilityState() {
    return [
      'visible' => [
        ':input[config-enforce-dependencies="' . $this->getEnforceDependenciesAttribute() . '"]' => ['checked' => TRUE],
        ':input[config-enforce-enabled="' . $this->getEnabledAttribute() . '"]' => ['checked' => TRUE],
      ],
    ];
  }

  /**
   * Create, update or delete enforced configs, as approriate.
   */
  protected function saveEnforcedConfigs() {
    $enforced_configs_to_create = $this->getEnforcedConfigsToCreate();
    if (!empty($enforced_configs_to_create)) {
      $this
        ->getEnforcedConfigCollection()
        ->createEnforcedConfigs($enforced_configs_to_create);
    }

    $enforced_configs_to_update = $this->getEnforcedConfigsToUpdate();
    if (!empty($enforced_configs_to_update)) {
      $this
        ->getEnforcedConfigCollection()
        ->updateEnforcedConfigs($enforced_configs_to_update);
    }

    $enforced_configs_to_delete = $this->getEnforcedConfigsToDelete();
    if (!empty($enforced_configs_to_delete)) {
      $this
        ->getEnforcedConfigCollection()
        ->deleteEnforcedConfigs($enforced_configs_to_delete);
    }
  }

  /**
   * Return a list of enforced configs to create.
   */
  protected function getEnforcedConfigsToCreate() {
    $enforced_configs = $this->getEnforcedConfigCollection()->getEnforcedConfigs();
    $enforced_configs_to_create = [];
    foreach ($this->getFormConfigs() as $config) {
      $this->setCurrentConfig($config);
      // This config isn't supposed to be enforced; move along.
      if (!$this->getEnforced()) continue;
      // If this config is already enforced, there's nothing to create.
      if (array_key_exists($config, $enforced_configs)) continue;

      $enforced_configs_to_create[$config] = [
        'target_module' => $this->getTargetModule(),
        'config_directory' => $this->getConfigDirectory(),
        'enforcement_level' => $this->getEnforcementLevel(),
        'config_form_uri' => $this->getConfigFormUri(),
      ];

      foreach ($this->getConfigDependencies() as $dependency) {
        if (!$this->getDependencyEnforced($dependency)) continue;

        $enforced_configs_to_create[$dependency] = [
          'target_module' => $this->getTargetModule(),
          'config_directory' => $this->getConfigDirectory(),
          'enforcement_level' => $this->getEnforcementLevel(),
        ];
      }
    }
    return $enforced_configs_to_create;
  }

  /**
   * Return a list of enforced configs to update.
   */
  protected function getEnforcedConfigsToUpdate() {
    $enforced_configs = $this->getEnforcedConfigCollection()->getEnforcedConfigs();
    $enforced_configs_to_update = [];
    foreach ($this->getFormConfigs() as $config) {
      $this->setCurrentConfig($config);
      // This config isn't supposed to be enforced; move along.
      if (!$this->getEnforced()) continue;
      // If this config isn't already enforced, there's nothing to update.
      if (!array_key_exists($config, $enforced_configs)) continue;

      $enforced_configs_to_update[$config] = [
        'target_module' => $this->getTargetModule(),
        'config_directory' => $this->getConfigDirectory(),
        'enforcement_level' => $this->getEnforcementLevel(),
        'config_form_uri' => $this->getConfigFormUri(),
      ];

      foreach ($this->getConfigDependencies() as $dependency) {
        if (!$this->getDependencyEnforced($dependency)) continue;

        $enforced_configs_to_update[$dependency] = [
          'target_module' => $this->getTargetModule(),
          'config_directory' => $this->getConfigDirectory(),
          'enforcement_level' => $this->getEnforcementLevel(),
        ];
      }
    }
    return $enforced_configs_to_update;
  }

  /**
   * Return a list of enforced configs to delete.
   */
  protected function getEnforcedConfigsToDelete() {
    $enforced_configs = $this->getEnforcedConfigCollection()->getEnforcedConfigs();
    $enforced_configs_to_delete = [];
    foreach ($this->getFormConfigs() as $config) {
      $this->setCurrentConfig($config);
      // This config is supposed to be enforced; move along.
      if ($this->getEnforced()) continue;
      // If this config isn't already enforced, there's nothing to delete.
      if (!array_key_exists($config, $enforced_configs)) continue;
      $enforced_configs_to_delete[$config] = $enforced_configs[$config];
    }
    return $enforced_configs_to_delete;
  }

  /**
   * Return the form URI, if appropriate.
   */
  protected function getConfigFormUri() {
    // Unless we're in an embedded enforce form, we can't figure out the correct URI.
    // @see EmbeddedEnforceForm::getConfigFormUri().
    return '';
  }

  /**
   * Return a list of directories for writing config.
   */
  protected function getAvailableConfigDirectories() {
    // @TODO Look this up in the storage service or make this a hook.
    return [
      InstallStorage::CONFIG_INSTALL_DIRECTORY => 'Install',
      InstallStorage::CONFIG_OPTIONAL_DIRECTORY => 'Optional',
    ];
  }

  /**
   * Return a human-readable name for a given config directory.
   */
  protected function getConfigDirectoryLabel($directory) {
    return $this->getAvailableConfigDirectories()[$directory];
  }

  /**
   * Return a list of all config object names from given module and directory.
   */
  protected function getModuleConfigNames($module, $directory) {
    $module_config_path = drupal_get_path('module', $module) . '/' . $directory;
    $storage = new FileStorage($module_config_path, StorageInterface::DEFAULT_COLLECTION);
    return $storage->listAll();
  }

  /**
   * Return array of config enforce values from the form.
   */
  protected function getEnforcedConfigValuesFromForm() {
    $values = $this->formState()->getValue('config_enforce');

    // Some forms appear to wrap our settings in an array.
    if (array_key_exists('config_enforce', $values)) {
      $values = $values['config_enforce'];
    }

    return $values;
  }

  /**
   * Return the value of a specific field for the current config object.
   */
  protected function getEnforcedConfigValue(string $field) {
    $values = $this->getEnforcedConfigValuesFromForm();
    return $values[$this->getCurrentConfig()][$field];
  }

  /**
   * Determine whether a value of a specific field for the current config object is set.
   */
  protected function enforcedConfigValueExists(string $field) {
    $values = $this->getEnforcedConfigValuesFromForm();
    return array_key_exists($field, $values[$this->getCurrentConfig()]);
  }

  /**
   * Return the new enabled setting from the form state.
   */
  protected function getEnforced() {
    return $this->getEnforcedConfigValue('config_enforce_enabled');
  }

  /**
   * Return the new target module from the form state.
   */
  protected function getTargetModule() {
    return $this->getEnforcedConfigValue('target_module');
  }

  /**
   * Return the new config directory from the form state.
   */
  protected function getConfigDirectory() {
    return $this->getEnforcedConfigValue('config_directory');
  }

  /**
   * Return the new enforcement level from the form state.
   */
  protected function getEnforcementLevel() {
    return $this->getEnforcedConfigValue('enforcement_level');
  }

  /**
   * Return whether to enforce dependencies based on the form state.
   */
  protected function getEnforceDependencies() {
    if (!$this->enforcedConfigValueExists('enforce_dependencies')) return [];
    return $this->getEnforcedConfigValue('enforce_dependencies');
  }

  /**
   * Return whether to enforce a given dependency based on the form state.
   */
  protected function getDependencyEnforced($dependency) {
    return $this->getEnforcedConfigValue('dependencies')[$dependency];
  }

  /**
   * Return the basic definition for a target module field.
   */
  protected function getTargetModuleField() {
    return [
      '#type' => 'select',
      '#title' => $this->t('Target module'),
      '#description' => "The module to which config should be written.",
      // Used cached value generated by scanning for `enforced_configs`
      '#options' => $this
        ->getEnforcedConfigCollection()
        ->getTargetModuleCollection()
        ->getTargetModuleLabels(),
      '#default_value' => $this
        ->getEnforcedConfigCollection()
        ->getTargetModuleCollection()
        ->getDefaultTargetModule(),
    ];
  }

  /**
   * Return the basic definition for a config directory field.
   */
  protected function getConfigDirectoryField() {
    $defaults = $this->config('config_enforce_devel.settings')->get('defaults');
    $key = 'config_directory';
    $default = is_array($defaults) && array_key_exists($key, $defaults) ? $defaults[$key] : InstallStorage::CONFIG_OPTIONAL_DIRECTORY;
    return [
      '#type' => 'select',
      '#title' => $this->t('Directory'),
      '#description' => "The config directory to which config should be written.",
      '#options' => $this->getAvailableConfigDirectories(),
      '#default_value' => $default,
    ];
  }

  /**
   * Return the basic definition for an enforcement level field.
   */
  protected function getEnforcementLevelField() {
    $defaults = $this->config('config_enforce_devel.settings')->get('defaults');
    $key = 'enforcement_level';
    $default = is_array($defaults) && array_key_exists($key, $defaults) ? $defaults[$key] : ConfigEnforcer::CONFIG_ENFORCE_READONLY;
    return [
      '#type' => 'select',
      '#title' => $this->t('Enforcement level'),
      '#description' => "The level of enforcement to apply to config objects.",
      '#options' => ConfigEnforcer::getEnforcementLevels(),
      '#default_value' => $default,
    ];
  }

  /**
   * Add a checkbox to control whether to enforce dependent config objects.
   */
  protected function getEnforceDependenciesField() {
    $defaults = $this->config('config_enforce_devel.settings')->get('defaults');
    $key = 'enforce_dependencies';
    $default = is_array($defaults) && array_key_exists($key, $defaults) ? $defaults[$key] : TRUE;
    return [
      '#type' => 'checkbox',
      '#title' => 'Enforce dependencies',
      '#description' => "Generate enforcement settings for the config objects upon which this one depends.",
      '#default_value' => $default,
    ];
  }

  /**
   * List unenforced config object names that have not been flagged for ignoring.
   */
  protected function getUnenforcedConfigs() {
    $unenforced_configs = $this->getAllUnenforcedConfigs();
    $ignored_configs = $this->getIgnoredConfigs();
    $filtered_configs = array_diff($unenforced_configs, $ignored_configs);
    return array_combine($filtered_configs, $filtered_configs);
  }

  /**
   * List all unenforced config object names.
   */
  protected function getAllUnenforcedConfigs() {
    $active_storage_configs = $this->configFactory()->listAll();
    $enforced_configs = array_keys($this->getEnforcedConfigCollection()->getEnforcedConfigs());
    $unenforced_configs = array_diff($active_storage_configs, $enforced_configs);
    return array_combine($unenforced_configs, $unenforced_configs);
  }

  /**
   * List ignored config objects.
   */
  protected function getIgnoredConfigs() {
    $ignored_configs = $this->config('config_enforce_devel.settings')->get('ignored_configs');
    return is_null($ignored_configs) ? [] : array_combine($ignored_configs, $ignored_configs);
  }

  /**
   * Return a list of all config objects that the current config object depends upon.
   */
  protected function getConfigDependencies($known_deps = []) {
    $config = $this->getCurrentConfig();
    $all_deps = $this->config($config)->get('dependencies');
    if (empty($all_deps)) return [];
    $config_deps = array_key_exists('config', $all_deps) ? $all_deps['config'] : [];
    // Compile a list of known dependencies, so that we can avoid infinite recursion below.
    $known_deps = array_merge($known_deps, $config_deps);
    foreach ($config_deps as $config_dep) {
      // Temporarily set the current config to the dependency.
      $this->setCurrentConfig($config_dep);
      // Recurse to ensure we're finding all dependencies.
      $new_deps = $this->getConfigDependencies($known_deps);
      // Strip out any dependencies that we've already found.
      $new_deps = array_diff($new_deps, $known_deps);
      $config_deps = array_merge($config_deps, $new_deps);
    }
    // Reset the current config to our original config.
    $this->setCurrentConfig($config);
    return $config_deps;
  }

}
