<?php

namespace Drupal\config_enforce_devel\Form;

use Drupal\config_enforce\Form\AbstractEnforceForm;
use Drupal\config_enforce_devel\Form\DevelFormHelperTrait;
use Drupal\config_enforce_devel\Form\ModalFormHelperTrait;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a settings form for the Config Enforce Devel module.
 */
class ModalEnforceForm extends AbstractEnforceForm {

  use ModalFormHelperTrait;

  const FORM_ID = 'config_enforce_devel_modal';
  const MODAL_TITLE = 'Config Enforce settings for: %arg';
  const REDIRECT_ROUTE = 'config_enforce_devel.enforced_configs';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $arg = '') {
    $this->setSharedFormProperties($form, $form_state);
    $this->config_name = $arg;

    // Mirror the structure of embedded forms.
    $this->form()['config_enforce'] = [
      '#type' => 'container',
      '#tree' =>TRUE,
    ];

    $this->makeFormModal();

    // While embedded enforce forms could theoretically have to handle multiple
    // config object, the modal stand-alone form is only designed to handle a
    // single config object's enforcement settings.
    $this->addEnforcedConfigItem($this->config_name);

    // Override the 'details' collapsible block.
    $this->form()['config_enforce'][$this->config_name]['#type'] = 'container';

    $this->form()['#attached']['library'][] = 'config_enforce_devel/config-enforce-devel';

    return $this->form();
  }

  /**
   * {@inheritdoc}
   */
  protected function getFormConfigs() {
    // Embedded forms get this from context. Here it was passed-in directly.
    // @see buildForm().
    return [$this->config_name];
  }

}
