<?php

namespace Drupal\config_enforce_devel\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\config_enforce_devel\EnforcedConfig;
use Drupal\config_enforce_devel\EnforcedConfigCollection;
use Drupal\config_enforce_devel\EnforcedConfigRegistry;
use Drupal\config_enforce_devel\TargetModuleCollection;
use Drupal\config_enforce_devel\Form\DevelFormHelperTrait;
use Drupal\config_enforce_devel\Form\ModalFormHelperTrait;

/**
 * Select which config objects to generate settings for.
 */
abstract class GenerateFormBase extends FormBase {

  use ModalFormHelperTrait;

  const REDIRECT_ROUTE = 'config_enforce_devel.enforced_configs';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->setSharedFormProperties($form, $form_state);

    // @TODO Switch to extending ConfirmFormBase, and fix the confirmation workflow.
    if ($this->isPendingConfirmation()) {
      $this->form = parent::buildForm($this->form(), $this->formState());
      $this->makeConfirmFormModal();
      return $this->form();
    }

    $this->makeFormModal();

    $this->form()['#attached']['library'][] = 'config_enforce_devel/config-enforce-devel';

    $this->form()['generate'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#title' => $this->t('Generate config enforce settings'),
      '#description' => $this->t('TBD.'),
      '#attributes' => [
        'id' => 'data-wrapper-' . $this->getFormId(),
      ],
    ];

    return $this->form();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->setSharedFormProperties($form, $form_state);

    if ($this->needsConfirmation()) return;

    $this->generateEnforcedConfigSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return 'Confirm generation of config enforcement settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('config_enforce_devel.enforced_configs');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Create enforcement settings for the following config objects: @list', [
      '@list' => $this->getConfigsHtmlList(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return 'Confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return 'Cancel';
  }

  /**
   * Add field to select one or more config objects to enforce.
   */
  protected function addTargetModuleField() {
    $this->form()['generate']['target_module'] = $this->getTargetModuleField();
  }

  /**
   * Add field to select one or more config objects to enforce.
   */
  protected function addConfigDirectoryField() {
    $this->form()['generate']['config_directory'] = $this->getConfigDirectoryField();
  }

  /**
   * Add select list to choose the enforcement level to apply to generated configs.
   */
  protected function addEnforcementLevelField() {
    $this->form()['generate']['enforcement_level'] = $this->getEnforcementLevelField();
  }

  /**
   * Return an array of configs to generate with their respective settings.
   */
  abstract protected function getConfigsToGenerate();

  /**
   * Returns an HTML-formatted list of config enforcements to generate.
   */
  protected function getConfigsHtmlList() {
    return $this->renderHtmlList(array_keys($this->getConfigsToGenerate()));
  }

  /**
   * Create config enforcement settings for selected config objects.
   */
  protected function generateEnforcedConfigSettings() {
    $new_enforced_configs = $this->getConfigsToGenerate();
    if (empty($new_enforced_configs)) return;
    $this->getEnforcedConfigCollection()->createEnforcedConfigs($new_enforced_configs);

    $this->messenger()->addMessage(
      $this->t('Generated enforcement settings for: %config_list', [
        '%config_list' => $this->renderHtmlList(array_keys($new_enforced_configs)),
      ])
    );
  }

}
