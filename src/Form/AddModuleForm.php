<?php

namespace Drupal\config_enforce_devel\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\config_enforce_devel\Form\ModalFormHelperTrait;
use Drupal\config_enforce_devel\Form\DevelFormHelperTrait;
use Drupal\config_enforce_devel\TargetModuleBuilder;

/**
 * Modal form and related logic to create a new target module.
 */
class AddModuleForm extends FormBase {

  use ModalFormHelperTrait;

  const FORM_ID = 'config_enforce_devel_add_module_form';
  const MODAL_TITLE = 'Create new target module';
  const REDIRECT_ROUTE = 'config_enforce_devel.settings';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->setSharedFormProperties($form, $form_state);

    $this->makeFormModal();
    $this->form()['actions']['send']['#value'] = $this->t('Create module');

    $this->form()['add_module'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];
    $this->form()['add_module']['name'] = [
      '#type' => 'textfield',
      '#title' => 'Name',
      '#description' => 'The name of the new target module.',
      '#required' => TRUE,
    ];
    $this->form()['add_module']['machine_name'] = [
      '#type' => 'textfield',
      '#title' => 'Machine name',
      '#description' => 'The machine name of the new target module.',
      '#required' => TRUE,
    ];
    $this->form()['add_module']['description'] = [
      '#type' => 'textfield',
      '#title' => 'Description',
      '#description' => 'The description to include in the new target module.'
    ];
    $this->form()['add_module']['path'] = [
      '#type' => 'textfield',
      '#title' => 'Modules path',
      '#default_value' => 'modules/custom',
      '#description' => 'The path in which to create the new target module.',
      '#required' => TRUE,
    ];

    return $this->form();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->setSharedFormProperties($form, $form_state);
    parent::validateForm($this->form(), $this->formState());

    $this->initializeTargetModule();

    // Ensure module name is unique.
    if (!$this->targetModule->nameIsUnique()) {
      $this->formState()
        ->setErrorByName('add_module][name', $this->t('Module name must be unique.'));
    }
    if (!$this->targetModule->machineNameIsUnique()) {
      $this->formState()
        ->setErrorByName('add_module][machine_name', $this->t('Machine name must be unique.'));
    }
    // Ensure module path does not exist.
    if ($this->targetModule->directoryPathExists()) {
      $this->formState()
        ->setErrorByName('add_module][path', $this->t('Module path (%path) already exists.', ['%path' => $path]));
    }
  }

  /**
   * Initialize a new target module from form values.
   */
  protected function initializeTargetModule() {
    $form_values = $this->formState()->getValue('add_module');
    $this->targetModule = new TargetModuleBuilder();
    $this->targetModule
      ->setName($form_values['name'])
      ->setMachineName($form_values['machine_name'])
      ->setDescription($form_values['description'])
      ->setPath($form_values['path']);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->setSharedFormProperties($form, $form_state);

    $this->initializeTargetModule();

    $this->targetModule
      ->createModule()
      ->installModule()
      ->registerAsTargetModule();
  }

}
