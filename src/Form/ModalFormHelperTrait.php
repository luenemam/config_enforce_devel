<?php

namespace Drupal\config_enforce_devel\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\config_enforce\Form\FormHelperTrait;

/**
 * Methods to simplify modal form handling.
 */
trait ModalFormHelperTrait {

  use DevelFormHelperTrait;

  protected $modalWidth = '800';

  /**
   * Callback for opening the modal form.
   */
  public function openModalForm($arg = '') {
    $response = new AjaxResponse();

    // Get the modal form using the form builder.
    $modal_form = \Drupal::formBuilder()->getForm(get_called_class(), $arg);

    // Add an AJAX command to open a modal dialog with the form as the content.
    $response->addCommand(new OpenModalDialogCommand($this->getModalTitle($arg), $modal_form, ['width' => $this->getModalWidth()]));

    return $response;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   */
  public function submitModalFormAjax(array $form, FormStateInterface $form_state) {
    $this->setSharedFormProperties($form, $form_state);

    $response = new AjaxResponse();

    // If there are any form errors, re-display the form.
    if ($this->formState()->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#' . $this->getFormId(), $this->form()));
    }
    else {
      // @TODO Figure out why this isn't getting called properly in the confirmation workflow.
      $response->addCommand(new CloseModalDialogCommand('#' . $this->getFormId()));
      $response->addCommand(new RedirectCommand($this->getRedirectUrl()));
    }

    return $response;
  }

  /**
   * AJAX callback handler that displays the confirmation form build.
   */
  public function confirmModalFormAjax(array $form, FormStateInterface $form_state) {
    $this->formState()->setRebuild();
    $this->setConfirmationStage('pending');
    $confirm_form = self::buildForm($this->form() , $this->formState());
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#' . $this->getFormId(), $confirm_form));
    return $response;
  }

  /**
   * Return the URL to redirect to after saving the modal form.
   */
  protected function getRedirectUrl() {
    return Url::fromRoute(get_called_class()::REDIRECT_ROUTE)
      ->toString();
  }

  /**
   * Return a title for the modal.
   */
  protected function getModalTitle(string $arg) {
    return $this->t(get_called_class()::MODAL_TITLE, ['%arg' => $arg]);
  }

  /**
   * Return the desired width of the modal.
   */
  protected function getModalWidth() {
    return $this->modalWidth;
  }

  /**
   * Prepare this form to be displayed in a modal dialog.
   */
  protected function makeFormModal($attach_actions = TRUE) {
    $this->form()['#prefix'] = "<div id='" . $this->getFormId() . "'>";
    $this->form()['#suffix'] = "</div>";
    $this->form()['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $this->form()['#attached']['library'][] = 'core/drupal.dialog.ajax';

    if ($attach_actions) {
      // Initialize confirmation workflow.
      if ($this->isAConfirmForm() && $this->getConfirmationStage() === FALSE) {
        $this->setConfirmationStage('needed');
      }

      $this->form()['actions'] = ['#type' => 'actions'];
      $this->form()['actions']['send'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#attributes' => [
          'class' => [
            'use-ajax',
          ],
        ],
        '#ajax' => [
          'callback' => [$this, $this->getAjaxCallback()],
          'event' => 'click',
          'wrapper' => 'data-wrapper-' . $this->getFormId(),
        ],
      ];
    }
  }

  /**
   * Prepare a confirmation form to be displayed in a modal dialog.
   */
  protected function makeConfirmFormModal() {
    $this->makeFormModal(FALSE);
    $this->form()['actions']['submit']['#attributes'] = [
      'class' => [
        'use-ajax',
      ],
      '#ajax' => [
        'callback' => [$this, $this->getAjaxCallback()],
        'event' => 'click',
        'wrapper' => 'data-wrapper-' . $this->getFormId(),
      ],
    ];
  }

  /**
   * Return the appropriate AJAX callback, depending on whether confirmation is needed.
   */
  protected function getAjaxCallback() {
    if ($this->needsConfirmation()) return 'confirmModalFormAjax';
    return 'submitModalFormAjax';
  }

  /**
   * Determine whether a form requires confirmation.
   */
  protected function needsConfirmation() {
    if (!$this->isAConfirmForm()) return FALSE;
    return $this->getConfirmationStage() === 'needed';
  }

  /**
   * Determine whether a form is pending confirmation.
   */
  protected function isPendingConfirmation() {
    if (!$this->isAConfirmForm()) return FALSE;
    return $this->getConfirmationStage() === 'pending';
  }

  /**
   * Return the current confirmation stage, or FALSE, if unset.
   */
  protected function getConfirmationStage() {
    if (!isset($this->form()['#confirm'])) return FALSE;
    return $this->form()['#confirm'];
  }

  /**
   * Set the confirmation stage.
   */
  protected function setConfirmationStage($stage) {
    $this->form()['#confirm'] = $stage;
  }

  /**
   * Determine whether a form supports a confirmation step.
   */
  protected function isAConfirmForm() {
    return in_array('Drupal\Core\Form\ConfirmFormInterface', class_implements(get_called_class()));
  }

}
