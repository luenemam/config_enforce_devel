<?php

namespace Drupal\config_enforce_devel\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\config_enforce\EnforcedConfig;
use Drupal\config_enforce_devel\Form\GenerateFormBase;
use Drupal\config_enforce_devel\TargetModuleCollection;

/**
 * Select which config objects to generate settings for.
 */
class GenerateFromModulesForm extends GenerateFormBase {

  const FORM_ID = 'config_enforce_devel_generate_from_modules_form';
  const MODAL_TITLE = 'Generate from modules';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    parent::buildForm($form, $form_state);

    $this->addEnforcementLevelField();
    $this->addSourceModulesField();

    return $this->form();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormName() {
    return 'config_enforce_devel_generate_from_modules_confirm_form';
  }

  /**
   * Add checkboxes for selected modules.
   */
  protected function addSourceModulesField() {
    $this->form()['generate']['source_modules'] = [
      '#type' => 'fieldgroup',
      '#tree' => TRUE,
      '#title' => $this->t('Source modules, directories and config objects'),
      '#description' => $this->t('Select config objects for which to generate enforcement settings.'),
    ];
    foreach ((new TargetModuleCollection())->getTargetModuleLabels() as $module => $label) {
      $this->addModule($module, $label);
    }
  }

  /**
   * Add a checkbox for a single module.
   */
  protected function addModule($module, $label) {

    $this->form()['generate']['modules'][$module]['generate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('%module (@module)', [
        '%module' => $label,
        '@module' => $module,
      ]),
      '#attributes' => [
        'name' => $module,
      ]
    ];
    $this->addModuleConfigDirectories($module);
  }

  /**
   * Add checkboxes for config directories.
   */
  protected function addModuleConfigDirectories($module) {
    $this->form()['generate']['modules'][$module]['config_dirs'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="' . $module . '"]' => ['checked' => TRUE],
        ],
      ],
    ];
    foreach ($this->getAvailableConfigDirectories() as $directory => $name) {
      $this->addModuleConfigDirectory($module, $directory);
    }
  }

  /**
   * Add a checkbox for a single config directory.
   */
  protected function addModuleConfigDirectory($module, $directory) {
    $config_dir = $module . '-' . $directory;
    $this->form()['generate']['modules'][$module]['config_dirs'][$directory]['generate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('%config_dir (@config_dir)', [
        '%config_dir' => $this->getConfigDirectoryLabel($directory),
        '@config_dir' => $directory,
      ]),
      '#attributes' => [
        'name' => $config_dir,
      ]
    ];
    $this->addModuleConfigs($module, $directory);
  }

  /**
   * Add a checkboxes for config objects from a config directory.
   */
  protected function addModuleConfigs($module, $directory) {
    $config_dir = $module . '-' . $directory;
    $this->form()['generate']['modules'][$module]['config_dirs'][$directory]['configs'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="' . $config_dir . '"]' => ['checked' => TRUE],
        ],
      ],
    ];
    foreach ($this->getModuleConfigNames($module, $directory) as $config_name) {
      $this->addModuleConfig($module, $directory, $config_name);
    }
  }

  /**
   * Add a checkbox for a single config object to enforce.
   */
  protected function addModuleConfig($module, $directory, $config_name) {
    $config_dir = $module . '-' . $directory;
    $this->form()['generate']['modules'][$module]['config_dirs'][$directory]['configs'][$config_name] = [
      '#type' => 'checkbox',
      '#title' => $config_name,
      '#states' => [
        'checked' => [
          ':input[name="' . $config_dir . '"]' => ['checked' => TRUE],
        ],
      ],
    ];

    if (EnforcedConfig::isEnforced($config_name)) {
      $this->form()['generate']['modules'][$module]['config_dirs'][$directory]['configs'][$config_name]['#title'] .= $this->t(' (already enforced)');
      $this->form()['generate']['modules'][$module]['config_dirs'][$directory]['configs'][$config_name]['#disabled'] = TRUE;
      $this->form()['generate']['modules'][$module]['config_dirs'][$directory]['configs'][$config_name]['#default_value'] = FALSE;
      unset($this->form()['generate']['modules'][$module]['config_dirs'][$directory]['configs'][$config_name]['#states']);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfigsToGenerate() {
    $generate = $this->formState()->getValue('generate');
    $enforced_configs = [];
    foreach ($generate['modules'] as $module => $module_data) {
      if (empty($module_data['config_dirs'])) continue;
      foreach ($module_data['config_dirs'] as $config_dir => $config_dir_data) {
        if (empty($config_dir_data['configs'])) continue;
        foreach ($config_dir_data['configs'] as $config => $checked) {
          if (!$checked) continue;
          $enforced_configs[$config] = [
             'target_module' => $module,
             'config_directory' => $config_dir,
             'enforcement_level' => $generate['enforcement_level'],
          ];
        }
      }
    }
    return $enforced_configs;
  }

}
