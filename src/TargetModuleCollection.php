<?php

namespace Drupal\config_enforce_devel;

use Drupal\config_enforce\TargetModuleCollection as ReadOnlyTargetModuleCollection;
use Drupal\config_enforce\ConfigEnforceHelperTrait;

/**
 * A collection of all target modules.
 */
class TargetModuleCollection extends ReadOnlyTargetModuleCollection {

  // Use log() method and related traits.
  use ConfigEnforceHelperTrait;

  // Channel with which to log from this class.
  const LOGCHANNEL = 'config_enforce_devel\TargetModuleCollection';

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Ensure that associated enforced configs registries exist for given modules.
   */
  public function ensureTargetModulesAreRegistered($modules) {
    $current_target_modules = $this->getTargetModuleNames();

    $new_target_modules = array_diff($modules, $current_target_modules);
    foreach ($new_target_modules as $module) {
      $this->registerNewTargetModule($module);
    }

    $old_target_modules = array_diff($current_target_modules, $modules);
    foreach ($old_target_modules as $module) {
      $this->unregisterOldTargetModule($module);
    }
    drupal_static_reset('Drupal\config_enforce\TargetModuleCollection::getTargetModules');
  }

  /**
   * Return the default target module.
   */
  public function getDefaultTargetModule() {
    $this->ensureDefaultTargetModuleIsSet();
    return $this->getDefaultTargetModuleName();
  }

  /**
   * Return the name of the default target module.
   */
  public function getDefaultTargetModuleName() {
    return $this->configFactory()->get('config_enforce_devel.settings')->get('defaults')['target_module'];
  }

  /**
   * Ensure that there is a default target module installed and registered.
   */
  public function ensureDefaultTargetModuleIsSet() {
    // If there is already a default target module, there is nothing to do here.
    if ($this->getDefaultTargetModuleName()) return;

    $target_modules = $this->getTargetModuleNames();
    // If there is at least one target module, default to the first one.
    if (!empty($target_modules)) {
      return $this->saveDefaultTargetModule(array_shift($target_modules));
    }

    $module_name = 'config_enforce_default';
    (new TargetModuleBuilder())
      ->setName($this->t('Config Enforce - Default Target Module'))
      ->setMachineName($module_name)
      ->setDescription($this->t('This is a default target module created when Config Enforce Devel is installed for the first time. It can be deleted safely once a new target module has been created.'))
      ->setPath('modules/custom')
      ->createModule()
      ->installModule()
      ->registerAsTargetModule();

    return $this->saveDefaultTargetModule($module_name);
  }

  /**
   * {@inheritdoc}
   *
   * We override here to get a read/write TargetModule.
   */
  protected function newTargetModule($module) {
    return new TargetModule($module);
  }

  /**
   * Initialize enforced configs for a given target module.
   */
  protected function registerNewTargetModule($module) {
    (new TargetModuleBuilder())
      ->setName($this->getAllModules()[$module])
      ->setMachineName($module)
      ->initializeEnforcedConfigs();
  }

  /**
   * Delete enforced configs for a given target module.
   */
  protected function unregisterOldTargetModule($module) {
    // We cannot unregister the default target module without causing undesired behaviour.
    if ($module == $this->getDefaultTargetModule()) return;
    (new EnforcedConfigRegistry($module))->delete();
  }

  /**
   * Ensure that there is a default target module installed and registered.
   */
  protected function saveDefaultTargetModule(string $module) {
    $settings = $this->configFactory()->getEditable('config_enforce_devel.settings');
    $defaults = $settings->get('defaults');
    $defaults['target_module'] = $module;
    $settings->set('defaults', $defaults)->save();

    $message = $this->t('Set default target module to %module.', [
      '%module' => $module,
    ]);

    $this->log(self::LOGCHANNEL)->notice($message);
    $this->messenger()->AddMessage($message);

    // Reload the page, so that the default target module can apply.
    return header("Refresh:0");
  }

}
