<?php

namespace Drupal\config_enforce_devel;

use Drupal\config_enforce\ConfigEnforceHelperTrait;
use Drupal\Core\Config\InstallStorage;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Url;
use Drupal\config_enforce\ConfigEnforcer;

/**
 * Create and manage a target module.
 */
class TargetModuleBuilder {

  // Channel with which to log from this class.
  use ConfigEnforceHelperTrait;

  const LOGCHANNEL = 'config_enforce_devel\TargetModuleBuilder';

  // The target module's human-readable name.
  protected $name;

  // The target module's machine-readable name.
  protected $machineName;

  // The target module's description.
  protected $description;

  // The path to which target modules should be written.
  protected $path;

  // An instance of the Drupal file system service.
  protected $fileSystem;

  /**
   * A basic constructor method.
   */
  public function __construct() {
    $this->fileSystem = \Drupal::service('file_system');
  }

  /**
   * Set the target module's human-readable name.
   */
  public function setName(string $name) {
    $this->name = $name;
    return $this;
  }

  /**
   * Return the target module's human-readable name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Set the target module's machine-readable name.
   */
  public function setMachineName(string $machine_name) {
    $this->machineName = $machine_name;
    return $this;
  }

  /**
   * Return the target module's machine-readable name.
   */
  public function getMachineName() {
    return $this->machineName;
  }

  /**
   * Set the target module's description.
   */
  public function setDescription(string $description) {
    $this->description = $description;
    return $this;
  }

  /**
   * Return the target module's description.
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Set the path to which target modules should be written.
   */
  public function setPath(string $path) {
    $this->path = $path;
    return $this;
  }

  /**
   * Return the path to which target modules should be written.
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * Determine whether the human-readable name is unique.
   */
  public function nameIsUnique() {
    return !in_array($this->getName(), (new TargetModuleCollection())->getAllModules());
  }

  /**
   * Determine whether the machine name is unique.
   */
  public function machineNameIsUnique() {
    return !array_key_exists($this->getMachineName(), (new TargetModuleCollection())->getAllModules());
  }

  /**
   * Determine whether the target module directory already exists.
   */
  public function directoryPathExists() {
    return is_dir($this->getDirectoryPath());
  }

  /**
   * Create a new target module.
   */
  public function createModule() {
    $this->prepareDirectory();
    $this->writeInfoFile();

    $message = $this->t('Created %name (@machine_name) module at %path.', [
      '%name' => $this->getName(),
      '@machine_name' => $this->getMachineName(),
      '%path' => $this->getDirectoryPath(),
    ]);
    $this->log(self::LOGCHANNEL)->notice($message);
    $this->messenger()->addStatus($message);

    return $this;
  }

  /**
   * Enable the new target module.
   */
  public function installModule() {
    \Drupal::service('module_installer')->install([$this->getMachineName()]);

    $message = $this->t('Installed %name (@machine_name) module.', [
      '%name' => $this->getName(),
      '@machine_name' => $this->getMachineName(),
    ]);
    $this->log(self::LOGCHANNEL)->notice($message);

    return $this;
  }

  /**
   * Register the new module as a target for writing config.
   */
  public function registerAsTargetModule() {
    $this->initializeEnforcedConfigs();
    drupal_static_reset('Drupal\config_enforce\TargetModuleCollection::getTargetModules');
    return $this;
  }

  /**
   * Create the initial enforced configs for the target module.
   */
  public function initializeEnforcedConfigs() {
    $module = $this->getMachineName();
    $registry = new EnforcedConfigRegistry($module);
    $config_name = $registry->getConfigName();

    // Enforced configs settings must, themselves, be enforced.
    $enforced_configs_to_create = [
      $config_name => [
        // Enforced configs settings are specific to their target module.
        'target_module' => $module,
        // Enforced configs settings must be imported when the terget module is enabled.
        'config_directory' => InstallStorage::CONFIG_INSTALL_DIRECTORY,
        // Enforced configs settings must, themselves, be read-only.
        'enforcement_level' => ConfigEnforcer::CONFIG_ENFORCE_READONLY,
        // While read-only, the enforced configs page is the form where this'll be embedded.
        'config_form_uri' => Url::fromRoute('config_enforce_devel.enforced_configs')->toString(),
      ],
    ];

    $registry->createEnforcedConfigs($enforced_configs_to_create);
    (new TargetModule($module))->initializeNewConfigFiles();

    $message = $this->t('Created config object %config_name for %name (@machine_name) target module enforced configs.', [
      '%config_name' => $config_name,
      '%name' => $this->getName(),
      '@machine_name' => $module,
    ]);
    $this->log(self::LOGCHANNEL)->notice($message);

    return $this;
  }

  /**
   * Ensure that a given directory exists and has the proper permissions.
   *
   * This defaults to the target module's directory, but can be overridden by
   * providing an argument.
   *
   * @param string $directory
   *   (Optional) A directory within the file path to create.
   */
  protected function prepareDirectory(string $directory = '') {
    $options = FileSystemInterface::CREATE_DIRECTORY + FileSystemInterface::MODIFY_PERMISSIONS;
    $directory_path = $this->getDirectoryPath();
    if (!empty($directory)) {
      $directory_path = $directory_path . '/' . $directory;
    }
    $this->fileSystem->prepareDirectory($directory_path, $options);

    return $this;
  }

  /**
   * Return the path of the new target module directory.
   */
  protected function getDirectoryPath() {
    return $this->getPath() . '/' . $this->getMachineName();
  }

  /**
   * Write a module info file.
   */
  protected function writeInfoFile() {
    $this->fileSystem
      ->saveData($this->getInfoFileContents(), $this->getInfoFilePath(), FileSystemInterface::EXISTS_REPLACE);

    return $this;
  }

  /**
   * Return the path of the new target module's info file.
   */
  protected function getInfoFilePath() {
    return $this->getDirectoryPath() . '/' . $this->getMachineName() . '.info.yml';
  }

  /**
   * Return a rendered the info file template.
   */
  protected function getInfoFileContents() {
    $template = drupal_get_path('module', 'config_enforce_devel') . '/templates/info.yml.twig';
    // Cast to string since twig_render_template returns a Markup object.
    return (string) \Drupal::service('twig')
      ->loadTemplate($template)
      ->render([
        'name' => $this->getName(),
        'description' => $this->getDescription(),
        // Needed to prevent notices when Twig debugging is enabled.
        'theme_hook_original' => 'not-applicable',
      ]);
  }

}
