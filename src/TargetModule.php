<?php

namespace Drupal\config_enforce_devel;

use Drupal\config_enforce\TargetModule as ReadOnlyTargetModule;

/**
 * A module to which config objects can be written.
 */
class TargetModule extends ReadOnlyTargetModule {

  // Channel with which to log from this class.
  const LOGCHANNEL = 'config_enforce_devel\TargetModule';

  /**
   * {@inheritdoc}
   *
   * We override here to ensure that we get a read/write registry.
   */
  public function __construct($machine_name) {
    $this->registry = new EnforcedConfigRegistry($machine_name);
    $this->machineName = $machine_name;
  }

  /**
   * Delete a set of config object files from disk.
   *
   * @param $enforced_configs_to_delete array of enforced configs to delete from disk.
   */
  public function deleteEnforcedConfigFiles($enforced_configs_to_delete) {
    foreach ($enforced_configs_to_delete as $config_name => $settings) {
      (new EnforcedConfigFile($config_name, $settings))->delete();
    }
  }

  /**
   * Create a new config file.
   *
   * Config Devel is generally responsible for writing enforced config to disk.
   * However, when we enable a config for the first time, it won't yet be in
   * the list of enforced configs the Config Devel should write to disk. So, we
   * have to write it ourselves this time.
   *
   * @see ConfigDevelHelper::autoExport()
   */
  public function initializeNewConfigFiles() {
    $enforced_configs = $this->getRegistry()->getEnforcedConfigs();
    foreach ($enforced_configs as $config_name => $settings) {
      if (!file_exists($settings['config_file_path'])) {
        (new EnforcedConfigFile($config_name, $settings))->initialize();
      }
    }
  }

}
