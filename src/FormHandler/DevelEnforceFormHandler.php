<?php

namespace Drupal\config_enforce_devel\FormHandler;

use Drupal\config_enforce\FormHandler\AbstractEnforceFormHandler;
use Drupal\config_enforce_devel\TargetModuleCollection;

/**
 * Attach an embedded enforce form to a host config form.
 */
class DevelEnforceFormHandler extends AbstractEnforceFormHandler {

  /**
   * Method to call from implementations of hook_alter().
   */
  public function alter() {
    // Only operate on config forms.
    if (!$this->isAnEnforceableForm()) return;

    $this->addEnforceForm('Drupal\config_enforce_devel\Form\EmbeddedEnforceForm');
  }

}
