<?php

namespace Drupal\config_enforce_devel;

use Drupal\Component\Utility\Crypt;
use Drupal\config_enforce\ConfigEnforcer;
use Drupal\config_enforce\EnforcedConfig;
use Drupal\config_enforce\EnforcedConfigRegistry as ReadOnlyEnforcedConfigRegistry;

/**
 * A collection of enforced configs for a given target module.
 */
class EnforcedConfigRegistry extends ReadOnlyEnforcedConfigRegistry {

  // Channel with which to log from this class.
  const LOGCHANNEL = 'config_enforce_devel\EnforcedConfigRegistry';

  /**
   * A basic constructor method.
   */
  public function __construct(string $target_module) {
    parent::__construct($target_module);
  }

  /**
   * Return an editable instance of the module's stored enforced configs.
   */
  public function getEditableConfig() {
    return $this->configFactory()->getEditable($this->getConfigName());
  }

  /**
   * @param $enforced_configs_to_update sparse array of enforced config settings to update.
   */
  public function updateEnforcedConfigs(array $enforced_configs_to_update) {
    $registry_config = $this->configFactory()->getEditable($this->getConfigName());
    $enforced_configs = $current_configs = $registry_config->get('enforced_configs');

    foreach ($enforced_configs_to_update as $config_name => $new_settings) {
      $orig_settings = $current_configs[self::encode($config_name)];
      $new_settings = array_merge($orig_settings, $new_settings);

      // Check if the current config is the enforced_configs registry itself.
      if ($config_name == $this->getConfigName()) {
        // A registry config's contents will change as soon as it gets saved
        // below, and so will the file hash. As a result, we set the hash
        // explicitly, to avoid spurious changes.
        $new_settings['hash'] = 'intentionally_invalid';
      }
      // If we didn't get passed a new hash, generate one now.
      elseif (empty($enforced_configs_to_update[$config_name]['hash'])) {
        $contents = @file_get_contents($new_settings['config_file_path']);
        $new_settings['hash'] = Crypt::hashBase64($contents);
      }

      // Target module and config file path are derived at load time, so we don't save them.
      unset($new_settings['target_module']);
      unset($new_settings['config_file_path']);
      // Record the updated settings.
      $enforced_configs[self::encode($config_name)] = $new_settings;
    }

    ksort($enforced_configs);
    $registry_config->set('enforced_configs', $enforced_configs);

    // Check if configs have changed.
    if ($current_configs != $registry_config->get('enforced_configs')) {
      // Save the updated registry config.
      $registry_config->save();
      // @TODO: Log this?
    }

  }

  /**
   * Add enforcement settings for a given list of config objects.
   *
   * @param $enforced_configs_to_create sparse array of enforced config settings to update.
   */
  public function createEnforcedConfigs(array $enforced_configs_to_create) {
    if (empty($enforced_configs_to_create)) return;

    $registry_config = $this->configFactory()->getEditable($this->getConfigName());
    $enforced_configs = $registry_config->get('enforced_configs');

    foreach ($enforced_configs_to_create as $config_name => $settings) {
      $settings['config_file_path'] = (new EnforcedConfigFile($config_name, $settings))
        ->getDerivedConfigFilePath();

      // Check if the current config is the enforced_configs registry itself.
      if ($config_name == $this->getConfigName()) {
        // A registry config's contents will change as soon as it gets saved
        // below, and so will the file hash. As a result, we set the hash
        // explicitly, to avoid spurious changes.
        $settings['hash'] = 'intentionally_invalid';
      }
      // Otherwise, generate one now.
      else {
        $settings['hash'] = (new ConfigEnforcer())->generateHash($settings['config_file_path'], $config_name);
      }

      // Target module and config file path are derived at load time, so we don't save them.
      unset($settings['target_module']);
      unset($settings['config_file_path']);
      // Record the updated settings.
      $enforced_configs[self::encode($config_name)] = $settings;
    }

    ksort($enforced_configs);

    // Save the updated registry config.
    $registry_config
      ->set('enforced_configs', $enforced_configs)
      ->save();

    // @TODO: Log this?

  }

  /**
   * @param $enforced_configs_to_delete array of enforced config settings to delete.
   */
  public function deleteEnforcedConfigs(array $enforced_configs_to_delete) {
    if (empty($enforced_configs_to_delete)) return;

    $registry_config = $this->configFactory()->getEditable($this->getConfigName());
    $enforced_configs = $registry_config->get('enforced_configs');

    foreach ($enforced_configs_to_delete as $config_name => $settings) {
      // Remove the enforced config settings.
      unset($enforced_configs[self::encode($config_name)]);
    }

    ksort($enforced_configs);

    // Save the updated registry config.
    $registry_config
      ->set('enforced_configs', $enforced_configs)
      ->save();

    // @TODO: Log this?

  }

  /**
   * Delete this registry of enforced configs.
   */
  public function delete() {
    $config_file = EnforcedConfig::getConfigFilePath($this->getConfigName());
    $this->fileSystem->delete($config_file);
    $this->getEditableConfig()->delete();

    $module = $this->getTargetModule();
    $message = $this->t('Unregistered %name (@machine_name) as a target module.', [
      '%name' => (new TargetModuleCollection)->getTargetModuleLabel($module),
      '@machine_name' => $module,
    ]);
    $this->log(self::LOGCHANNEL)->notice($message);
  }

}
