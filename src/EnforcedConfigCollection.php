<?php

namespace Drupal\config_enforce_devel;

use Drupal\config_enforce\EnforcedConfigCollection as ReadOnlyEnforcedConfigCollection;

/**
 * A collection of all enforced configs with the ability to update enforcement settings.
 */
class EnforcedConfigCollection extends ReadOnlyEnforcedConfigCollection {

  // Channel with which to log from this class.
  const LOGCHANNEL = 'config_enforce_devel\EnforcedConfigCollection';

  /**
   * {@inheritdoc}
   *
   * We override here to ensure that we get a read/write TargetModuleCollection.
   */
  public function __construct() {
    $this->targetModuleCollection = new TargetModuleCollection();
  }

  /**
   * Update a given set of enforced config objects' enforcement settings.
   *
   * @param $enforced_configs_to_update array of enforced config settings that need to updated.
   */
  public function updateEnforcedConfigs(array $enforced_configs_to_update) {
    if (empty($enforced_configs_to_update)) return;

    // Build list of current settings for the configs we're about to update.
    $current_enforced_configs = array_intersect_key($this->getEnforcedConfigs(), $enforced_configs_to_update);
    // Fill in sparse array of configs to update with current settings, where they are otherwise missing.
    $enforced_configs_to_update = array_replace_recursive($current_enforced_configs, $enforced_configs_to_update);

    $module_enforced_configs_to_remove = [];
    $module_enforced_configs_to_create = [];

    foreach ($this->getTargetModuleCollection()->getTargetModules() as $target_module) {
      $registry = $target_module->getRegistry();

      // Only update those enforced configs in this target module.
      $module_enforced_configs_to_update = [];
      foreach ($enforced_configs_to_update as $config_name => $new_settings) {
        $old_settings = $current_enforced_configs[$config_name];
        if ($new_settings['target_module'] != $target_module->getMachineName()) continue;

        // If config directory or target module have changed, remove and re-create these settings.
        if ((new EnforcedConfigFile($config_name, $old_settings))->configFilePathIsChanged($new_settings)) {
          $module_enforced_configs_to_remove[$config_name] = $old_settings;
          $module_enforced_configs_to_create[$config_name] = $new_settings;

        // Otherwise just update them.
        } else {
          $module_enforced_configs_to_update[$config_name] = $new_settings;
        }

        // Drain the input array.
        unset($enforced_configs_to_update[$config_name]);
      }
      $registry->updateEnforcedConfigs($module_enforced_configs_to_update);
    }

    $this->deleteEnforcedConfigs($module_enforced_configs_to_remove);
    $this->createEnforcedConfigs($module_enforced_configs_to_create);

    // If we have any entries left in the input array, something's gone wrong.
    if (count($enforced_configs_to_update)) {
      // @TODO: Log this?
    }
  }

  /**
   * Register a given a set of config objects and enforcement settings for enforcement.
   *
   * @param $enforced_configs_to_create array of enforced config settings that need to created.
   */
  public function createEnforcedConfigs(array $enforced_configs_to_create) {
    if (empty($enforced_configs_to_create)) return;

    foreach ($this->getTargetModuleCollection()->getTargetModules() as $target_module) {
      $registry = $target_module->getRegistry();

      // Only create enforced configs for this target module.
      $module_enforced_configs_to_create = [];
      foreach ($enforced_configs_to_create as $config_name => $settings) {
        // We skip any enforced configs destined for other target modules, as
        // they'll be processed on a subsequent iteration.
        if ($settings['target_module'] != $target_module->getMachineName()) continue;

        $module_enforced_configs_to_create[$config_name] = $settings;

        // Drain the input array.
        unset($enforced_configs_to_create[$config_name]);
      }
      $registry->createEnforcedConfigs($module_enforced_configs_to_create);
      $target_module->initializeNewConfigFiles();
    }

    // If we have any entries left in the input array, something's gone wrong.
    if (count($enforced_configs_to_create)) {
      // @TODO: Log this?
    }
  }

  /**
   * Delete a given list of enforced configs from their registry and disk.
   *
   * @param $enforced_configs_to_delete array of enforced config settings that need to deleted.
   */
  public function deleteEnforcedConfigs(array $enforced_configs_to_delete) {
    if (empty($enforced_configs_to_delete)) return;

    foreach ($this->getTargetModuleCollection()->getTargetModules() as $target_module) {
      $registry = $target_module->getRegistry();

      // Only delete enforced configs for this target module.
      $module_enforced_configs_to_delete = [];
      foreach ($enforced_configs_to_delete as $config_name => $settings) {
        // We skip any enforced configs destined for other target modules, as
        // they'll be processed on a subsequent iteration.
	      if ($settings['target_module'] != $target_module->getMachineName()) continue;

        $module_enforced_configs_to_delete[$config_name] = $settings;

        // Drain the input array.
        unset($enforced_configs_to_delete[$config_name]);
      }
      $registry->deleteEnforcedConfigs($module_enforced_configs_to_delete);
      $target_module->deleteEnforcedConfigFiles($module_enforced_configs_to_delete);
    }

    // If we have any entries left in the input array, something's gone wrong.
    if (count($enforced_configs_to_delete)) {
      // @TODO: Log this?
    }
  }

}
