<?php

namespace Drupal\config_enforce_devel;

use Drupal\Core\Url;

/**
 * Toolbar integration handler.
 */
class ToolbarHandler {

  /**
   * Hook bridge.
   *
   * @return array
   *   The Config Enforce Devel toolbar item render array.
   *
   * @see hook_toolbar()
   */
  public function toolbar() {
    $items = [];

    $items['config_enforce_devel'] = [
      '#type' => 'toolbar_item',
      'tab' => [
        '#type' => 'link',
        '#title' => t('Enforced configs'),
        '#url' => Url::fromRoute('config_enforce_devel.enforced_configs'),
        '#options' => [
          'attributes' => [
            'class' => [
              'toolbar-item',
              'toolbar-icon',
              'toolbar-icon-config-enforce-devel',
            ],
          ],
        ],
      ],
      '#attached' => [
        'library' => 'config_enforce_devel/config-enforce-devel',
      ],
      '#weight' => 1000,
    ];

    return $items;
  }

}
